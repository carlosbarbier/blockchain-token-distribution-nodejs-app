/**
 * This demonstrates how to create an address with ethereumjs-wallet .
 */
const keccak256 = require("keccak256");
const bip39 = require("bip39");
const {hdkey} = require("ethereumjs-wallet");

(async function getHDWalletDetails()  {
    console.log("[Creating a wallet ... ]");
    const secret = "my secret key to create for my new wallet";

    const buffer = await bip39.mnemonicToSeed(secret);

    let hdWallet = hdkey.fromMasterSeed(buffer);
    const path  = "m/44'/60'/0'/0/0";
    const wallet = hdWallet.derivePath(path).getWallet();

    const publicKey  = wallet.getPublicKey();
    const hashedPubKey = keccak256(publicKey).toString('hex');
    const ethAddress = hashedPubKey.substring(24);

    console.log([`Ethereum Address is: 0x${ethAddress}`]);
})()

