# Blockchain Token Distribution Application

This project is an assignment for the students registered in the  Post Graduate Diploma in Cloud Computing course in National College of Ireland.
The objective of this is to develop an application with Node Js that distributes Ethereum ERC20 tokens on an Ethereum Testnet.
### Requirements

* Node js preferred version 14 or greater
* Have docker installed on your machine

### Environment variables
```
INFURA_TOKEN=
CONTRACT_ADDRESS=
OWNER_ADDRESS=
SUPER_SECRET_PRIVATE_KEY=
```

### Solidity Code
My solidity code can be found inside the folder contract named carlos_barbier.sol .


## How to run the application locally
1. Clone this repository
2. make sure that you are inside the directory
3. run npm install ```npm install```
4. run npm run dev ```npm run dev```


## How to run the application with docker locally
 Make sure to have Docker installed on your machine and the command.
 ```
 docker-compose up
 ```

## How to create  Ethereum addresses
Run the command
 ```
 npm run wallet
 ```

## ECDH Demonstration
Run the command
 ```
 npm run ecdh
 ```

## Docker container on Docker Hub
```
docker pull carloskb/kouakou-erc20:latest
```
## Usefully links
* My contract address: 0x549d1ed040371866b839d11689363feade379287
* My address: [address](https://ropsten.etherscan.io/address/0x972f2C3595fE6987A84f4637A157C8a1461c59C1)
* My token: [My Token](https://ropsten.etherscan.io/token/0x549d1ed040371866b839d11689363feade379287)

