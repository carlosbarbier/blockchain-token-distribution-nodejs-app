const Web3 = require("web3");
const Transaction = require("ethereumjs-tx").Transaction;

require('dotenv').config();

infuraToken = process.env.INFURA_TOKEN;
contractAddress = process.env.CONTRACT_ADDRESS;
ownerAddress = process.env.OWNER_ADDRESS;
privateKey = Buffer.from(process.env.SUPER_SECRET_PRIVATE_KEY, 'hex');


const rpcURL = "https://ropsten.infura.io/v3/" + infuraToken
console.log(`Infura URL: ${rpcURL}`);

const web3 = new Web3(rpcURL);
console.log("Connected to web3");

// load in contract's ABI
const abi=[{"inputs":[{"internalType":"string","name":"name_","type":"string"},{"internalType":"string","name":"symbol_","type":"string"}],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"owner","type":"address"},{"indexed":true,"internalType":"address","name":"spender","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Approval","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"from","type":"address"},{"indexed":true,"internalType":"address","name":"to","type":"address"},{"indexed":false,"internalType":"uint256","name":"value","type":"uint256"}],"name":"Transfer","type":"event"},{"inputs":[{"internalType":"address","name":"owner","type":"address"},{"internalType":"address","name":"spender","type":"address"}],"name":"allowance","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"approve","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"account","type":"address"}],"name":"balanceOf","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"decimals","outputs":[{"internalType":"uint8","name":"","type":"uint8"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"subtractedValue","type":"uint256"}],"name":"decreaseAllowance","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"spender","type":"address"},{"internalType":"uint256","name":"addedValue","type":"uint256"}],"name":"increaseAllowance","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"name","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"symbol","outputs":[{"internalType":"string","name":"","type":"string"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"totalSupply","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"address","name":"recipient","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"transfer","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"sender","type":"address"},{"internalType":"address","name":"recipient","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"}],"name":"transferFrom","outputs":[{"internalType":"bool","name":"","type":"bool"}],"stateMutability":"nonpayable","type":"function"}]
// create an instance of a contract object that
// - obeys the ABI
// - is at the address of our deployed contract
const contract = new web3.eth.Contract(abi, contractAddress);

async function  getName (){
    let name = await contract.methods.name().call();
    console.log(`Name is: ${name}`);
    return name;
}

async function getSymbol() {
    const symbol = await contract.methods.symbol().call();
    console.log(`Symbol is: ${symbol}`);
    return symbol;
}

async function  getTotalSupply () {
    const totalSupply = await contract.methods.totalSupply().call();
    console.log(`Total supplied is: ${totalSupply}`);
    return totalSupply;
}

async function  getBalance(address) {
    const  balance = await contract.methods.balanceOf(address).call();
    console.log(`Balance of ${address} address is : ${balance}`);
    return balance;
}

async function getDecimals() {
    const  decimals = await contract.methods.decimals().call();
    console.log(`Decimals is: ${decimals}`);
    return decimals;
}

async function transferToken (fromAddress, toAddress, amount) {
    const nonce = await web3.eth.getTransactionCount(fromAddress);
    console.log(`Nonce of ${nonce} for address :${fromAddress}`)

    const txObject = {
        nonce: web3.utils.toHex(nonce),
        gasLimit: web3.utils.toHex(500000),
        gasPrice: web3.utils.toHex(web3.utils.toWei('100', 'gwei')),
        to: contractAddress,
        data: contract.methods.transfer(toAddress, amount).encodeABI()
    }

    const tx = new Transaction(txObject, {chain: 'ropsten', hardfork: 'petersburg'});

    tx.sign(privateKey);

    const serializedTx = tx.serialize();
    const raw = '0x' + serializedTx.toString('hex');

    console.log("about to send tx");
    const  txResponse = await web3.eth.sendSignedTransaction(raw);

    console.log(`tx sent. block number is ${txResponse.blockNumber}`);
    console.log(`tx sent. tx hash is ${txResponse.transactionHash}`);
}


module.exports = {getName, getSymbol, getBalance, getDecimals, getTotalSupply, transferToken}
