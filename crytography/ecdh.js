/**
 * The purpose of this file is to demonstrate how encryption and decryption work.
 * For this demo we will use a popular library called sodium-native.
 * For the demo we will use two different person Bob and Karl, both have a private key and public key.
 */

const sodium = require("sodium-native");

console.log("[ECDH Demonstration ...]");

const publicKeyBytes = sodium.crypto_box_PUBLICKEYBYTES;
const privateKeyBytes = sodium.crypto_box_SECRETKEYBYTES;

/**
 * Creating Bob key Pay
 */
const  bobPrivateKey = sodium.sodium_malloc(privateKeyBytes);
const bobPublicKey = sodium.sodium_malloc(publicKeyBytes);

sodium.crypto_box_keypair(bobPublicKey, bobPrivateKey);
console.log("[Bob  key pair has been created successfully]");

/**
 * Creating Karl key pair
 */
const karlPrivateKey = sodium.sodium_malloc(privateKeyBytes);
const karlPublicKey = sodium.sodium_malloc(publicKeyBytes);

sodium.crypto_box_keypair(karlPublicKey, karlPrivateKey);

console.log("[Karl  key pair has been created successfully]");

const secretBytes = sodium.crypto_scalarmult_BYTES;

/**
 * Creating Bob secret
 */
const bobSecret = sodium.sodium_malloc(secretBytes);

/**
 * Clear memory location
 */
sodium.sodium_memzero(bobSecret);
sodium.crypto_scalarmult(bobSecret, bobPrivateKey, bobPublicKey);
console.log(`[Bob's secret is 0x${bobSecret.toString("hex")}]`);

/**
 * Creating Karl Secret
 */
const karlSecret = sodium.sodium_malloc(secretBytes);

/**
 * Clear memory location
 */
sodium.sodium_memzero(karlSecret);
sodium.crypto_scalarmult(karlSecret, karlPrivateKey, bobPublicKey);
console.log(`[Karl's secret is 0x${bobSecret.toString("hex")}]`);