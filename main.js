const fs = require("fs");
const BigNumber = require("big-number");
const {transferToken, getSymbol, getBalance, getName,} = require("./contract");

require('dotenv').config();

contractAddress = process.env.CONTRACT_ADDRESS;
ownerAddress = process.env.OWNER_ADDRESS;


/**
 * This function distributes tokens across all the account provided in the file account.txt
 * and keep the remaining to the token owner.
 * @returns {Promise<void>}
 */
(async function distributeToken() {
    const contractName= getName()
    console.log(`[Contract name: ${contractName}]`);

    const distributionAddresses = getAddresses()
    console.log(`[Distribution addresses available: ${distributionAddresses.length}]`);

    await getSymbol();

    const remainingSupply = await getBalance(ownerAddress);
    const ownerBalance = new BigNumber(remainingSupply);
    console.log(`[Owner balance is: ${ownerBalance}]`)

    const fivePercentOfOwnerBalance = ownerBalance.div(20);
    console.log(`[5% of remaining supplied is: ${fivePercentOfOwnerBalance}]`);

    const numberOfAddresses = distributionAddresses.length;
    const distributionAmount = fivePercentOfOwnerBalance.div(numberOfAddresses);

    const symbol = await getSymbol()

    for (let i = 0; i < numberOfAddresses; i++) {
        console.log(`[Distributing account ${i} : ${distributionAmount} ${symbol} to ${distributionAddresses[i]}]`)
        await transferToken(ownerAddress, distributionAddresses[i], distributionAmount)

    }

})()


function getAddresses() {
    let addresses
    try {

        addresses = fs
            .readFileSync("./accounts.txt", "utf8")
            .split(",");

    } catch (e) {
        console.error('Exception occurred when reading account.txt')
    }
    return addresses
}
